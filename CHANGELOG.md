CHANGELOG
=========

7.1.0
-----
- Suppression de la dépendance avec unicaen/app (l'envoi du jeton par mail est désormais à la charge de l'appli).

7.0.0
-----
- Requiert les nouvelles bibliotèques unicaen/authentification et privilege.

6.0.3
-----
- Envoi du token par mail : retrait de l'injection du MailerService dans le TokenService 
  car ça pouvait créer une boucle infinie de dépendances et ce n'était pas une bonne idée de toute façon 
  (TokenService n'a pas vocation à envoyer des mails). C'est TokenController qui envoie le mail désormais.

6.0.2
-----
- Remplacement des icones glyphicon par unicaen-icon

6.0.0
-----
- Correction de la version requise de unicaen/app et auth

5.0.1
-----
- Possibilité de passer à PHP 8
- [FIX] Module.php : activation redondante du ModuleRouteListener (à faire 1 fois uniquement dans le module Application)

5.0.0 (Bootstrap 5)
-----
- Passage possible à unicaen/app et unicaen/auth version 5 (Bootstrap 5).

4.0.1
-----
- [FIX] La classe d'hydrateur DoctrineObject a déménagé.

4.0.0
-----
- Passage à unicaen/app et unicaen/auth version 4.

1.0.0
-----
- Première version.
