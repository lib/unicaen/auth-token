Module unicaen/auth-token
=========================

L'origine de ce module est le besoin de fournir à des utilisateurs "extérieurs" (au SI de l'établissement)
un moyen simple de se connecter à une application.

Simple, c'est-à-dire sans avoir à créer un compte local à une appli car saisir un identifiant et un mot de passe 
est trop compliqué pour certaines personnes ! Donc en gros, le but est de pouvoir fournir un lien à cliquer 
contenant un jeton d'authentification. 

Une fois que l'on clique sur ce lien, on est mené à la page d'authentification fournie par unicaen/auth où il suffira
de cliquer sur un bouton (pfff, 2e clic) pour se voir authentifié au sein de l'appli.

Ce module s'appuie sur :
- le module `unicaen/auth` auquel il fournit un "adpater" d'authentification supplémentaire 
`token`.
- le module `unicaen/app` pour envoyer les jetons par mail.  

Côté fonctionnalité, ce module fournit de quoi :
- créer un jeton d'authentification pour un utilisateur ;
- lister les jetons ;
- prolonger la date d'expiration d'un jeton ;
- supprimer un jeton ;
- envoyer par mail le lien d'authentification utilisant un jeton.

Côté dev, ce module fournit :
- le script SQL pour créer les objets de base de données nécessaires (syntaxe postgres et oracle) ;
- une classe abstraite d'entité `AbstractUserToken` et la clé de config `'user_token_entity_class'` 
  permettant d'utiliser sa propre table/entité ;
- un service boîte à outils `TokenService`.
- des événements déclenchés avant et après les opérations majeures (avant/après l'enregistrement d'un jeton, 
  avant/après la suppression, etc.)
- des vues partielles ("partial") pour :
  - dessiner le bouton de création d'un jeton ;
  - afficher une liste de jetons dans un tableau.
  
Exemple dans un contrôleur :

```php
namespace Application\Controller;

use UnicaenAuthToken\Controller\TokenController;

class UtilisateurController extends \UnicaenUtilisateur\Controller\UtilisateurController implements SearchControllerInterface
{
    // ...
  
    /**
     * Ecoute de l'événement TokenController::EVENT_TOKEN_CREATE_AFTER_SAVE permettant 
     * d'agir après la création du jeton pour par exemple envoyer par mail le jeton à l'utilisateur
     * (cf {@see envoyerToken()}).
     */
    public function listenEventsOf(TokenController $tokenController)
    {
        // écoute pour envoyer automatiquement tout jeton nouvellement créé à l'utlisateur
        $tokenController->getEventManager()->attach(
            $tokenController::EVENT_TOKEN_CREATE_AFTER_SAVE,
            [$this, 'envoyerToken']
        );
    }
    
    public function envoyerToken(EventInterface $event)
    {
        /** @var \Application\Entity\Db\UtilisateurToken $utilisateurToken */
        $utilisateurToken = $event->getParam('userToken');
    
        // on délègue au module unicaen/auth-token
        $this->forward()->dispatch(TokenController::class, [
            'action' => 'envoyer',
            'userToken' => $utilisateurToken->getId(),
        ]);
    }
}
```

Exemple dans une vue :

```php
<div class="box panel panel-info">
    <div class="panel-heading">
        <h2 class="first">
            <?php echo $this->translate("Jeton d'authentification") ?>
            <div class="pull-right">
                <br>
                <?php echo $this->partial('unicaen-auth-token/token/partial/add-button', [
                    'redirect' => $redirect,
                    'user' => $utilisateur->getId(),
                ]) ?>
            </div>
        </h2>
    </div>
    <div class="panel-body">
        <?php if (empty($tokens)): ?>
            L'utilisateur n'a aucun jeton d'authentification.
        <?php else: ?>
            <?php echo $this->partial('unicaen-auth-token/token/partial/table', ['paginator' => $tokens, 'redirect' => $redirect]); ?>
        <?php endif ?>
    </div>
</div>
```
