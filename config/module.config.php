<?php

namespace UnicaenAuthToken;

use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use UnicaenAuthToken\Mvc\RedirectResponse;
use UnicaenAuthToken\Mvc\RedirectResponseFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenPrivilege\Provider\Rule\PrivilegeRuleProvider;
use UnicaenAuthToken\Authentication\Adapter\TokenAdapter;
use UnicaenAuthToken\Authentication\Adapter\TokenAdapterFactory;
use UnicaenAuthToken\Controller\TokenController;
use UnicaenAuthToken\Controller\TokenControllerFactory;
use UnicaenAuthToken\Entity\Db\AbstractUserToken;
use UnicaenAuthToken\Entity\Db\UserToken;
use UnicaenAuthToken\Form\TokenLoginForm;
use UnicaenAuthToken\Form\TokenLoginFormFactory;
use UnicaenAuthToken\Form\UserTokenForm;
use UnicaenAuthToken\Form\UserTokenFormFactory;
use UnicaenAuthToken\Hydrator\UserTokenHydrator;
use UnicaenAuthToken\Hydrator\UserTokenHydratorFactory;
use UnicaenAuthToken\Options\ModuleOptions;
use UnicaenAuthToken\Options\ModuleOptionsFactory;
use UnicaenAuthToken\Privilege\TokenPrivilege;
use UnicaenAuthToken\Service\TokenService;
use UnicaenAuthToken\Service\TokenServiceFactory;
use UnicaenAuthToken\View\Helper\TokenConnectViewHelper;
use UnicaenAuthToken\View\Helper\TokenConnectViewHelperFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'unicaen-auth-token' => [
        /**
         * Classe d'entité Doctrine mappant un jeton utilisateur en base de données.
         */
        'user_token_entity_class' => UserToken::class,
    ],

    'unicaen-auth' => [
        /**
         * Déclarartion du type d'authentification supplémentaire supporté.
         */
        'auth_types' => [
            'token',
        ],

        /**
         * Configuration de l'authentification à l'aide d'un token dans la BDD de l'application.
         */
        'token' => [
            /**
             * Activation ou non de ce mode d'authentification.
             */
            'enabled' => true,

            /**
             * Description facultative de ce mode d'authentification qui apparaîtra sur la page de connexion.
             */
            'description' => null,

            /**
             * Adapter compétent pour réaliser l'authentification de l'utilisateur.
             */
            'adapter' => TokenAdapter::class,

            /**
             * Service/formulaire d'authentification à utiliser.
             */
            'form' => TokenLoginForm::class,

            'order' => 100,
        ],
    ],

    'zfcuser'         => [
        'auth_adapters' => [
            10 => TokenAdapter::class,
        ],
    ],

    'bjyauthorize' => [
        'resource_providers' => [
            'BjyAuthorize\Provider\Resource\Config' => [
                AbstractUserToken::RESOURCE_ID => [],
            ],
        ],
        'rule_providers' => [
            PrivilegeRuleProvider::class => [
                'allow' => [
                    [
                        'privileges' => [
                            TokenPrivilege::CREER,
                            TokenPrivilege::MODIFIER,
                            TokenPrivilege::SUPPRIMER,
                            TokenPrivilege::PROLONGER,
                            TokenPrivilege::ENVOYER,
                        ],
                        'resources' => AbstractUserToken::RESOURCE_ID,
                    ],
                ],
            ],
        ],
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => TokenController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => TokenPrivilege::LISTER,
                ],
                [
                    'controller' => TokenController::class,
                    'action' => [
                        'creer',
                    ],
                    'privileges' => TokenPrivilege::CREER,
                ],
                [
                    'controller' => TokenController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => TokenPrivilege::MODIFIER,
                ],
                [
                    'controller' => TokenController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => TokenPrivilege::SUPPRIMER,
                ],
                [
                    'controller' => TokenController::class,
                    'action' => [
                        'prolonger',
                    ],
                    'privileges' => TokenPrivilege::PROLONGER,
                ],
                [
                    'controller' => TokenController::class,
                    'action' => [
                        'envoyer',
                    ],
                    'privileges' => TokenPrivilege::ENVOYER,
                ],
            ],
        ],
    ],

    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    __NAMESPACE__ . '\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/UnicaenAuthToken/Entity/Db/Mapping',
                ],
            ],
        ],
    ],

    'navigation' => [
        'default' => [
            'home' => [
                'pages' => [
                    'tokens' => [
                        'label' => "Jetons utilisateurs",
                        'route' => 'unicaen-auth-token/token',
                        'resource' => TokenPrivilege::getResourceId(TokenPrivilege::LISTER),
                        'order' => 100,
                        'visible' => false,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-auth-token' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/unicaen-auth-token',
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'token' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/token',
                            'defaults' => [
                                'controller' => TokenController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'creer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/creer[/:user]',
                                    'defaults' => [
                                        'action' => 'creer',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:userToken',
                                    'constraints' => [
                                        'userToken' => '\d+',
                                    ],
                                    'defaults' => [
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:userToken',
                                    'constraints' => [
                                        'userToken' => '\d+',
                                    ],
                                    'defaults' => [
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                            'prolonger' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/prolonger/:userToken',
                                    'constraints' => [
                                        'userToken' => '\d+',
                                    ],
                                    'defaults' => [
                                        'action' => 'prolonger',
                                    ],
                                ],
                            ],
                            'envoyer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/envoyer/:userToken',
                                    'constraints' => [
                                        'userToken' => '\d+',
                                    ],
                                    'defaults' => [
                                        'action' => 'envoyer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            ModuleOptions::class => ModuleOptionsFactory::class,
            TokenService::class => TokenServiceFactory::class,
            TokenLoginForm::class => TokenLoginFormFactory::class,
            TokenAdapter::class => TokenAdapterFactory::class,
            RedirectResponse::class => RedirectResponseFactory::class,
        ],
    ],

    'controllers' => [
        'factories' => [
            TokenController::class => TokenControllerFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [
            UserTokenForm::class => UserTokenFormFactory::class,
        ],
    ],

    'hydrators' => [
        'factories' => [
            UserTokenHydrator::class => UserTokenHydratorFactory::class,
        ],
    ],

    'view_helpers'  => [
        'aliases' => [
            'tokenConnect' => TokenConnectViewHelper::class,
        ],
        'factories' => [
            TokenConnectViewHelper::class => TokenConnectViewHelperFactory::class,
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
