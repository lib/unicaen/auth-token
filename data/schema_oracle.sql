--
-- Table, etc.
--

create table USER_TOKEN
(
    ID NUMBER not null
        constraint USER_TOKEN_PK
            primary key,
    USER_ID NUMBER not null,
    TOKEN VARCHAR2(256) not null,
    ACTION VARCHAR2(256),
    ACTIONS_COUNT NUMBER default 0 not null,
    ACTIONS_MAX_COUNT NUMBER default 0 not null,
    CREATED_ON DATE default sysdate not null,
    EXPIRED_ON DATE not null,
    LAST_USED_ON DATE,
    SENT_ON DATE
)
;
comment on table USER_TOKEN is 'Jetons d''authentification utilisateur'
;
comment on column USER_TOKEN.USER_ID is 'Identifiant unique de l''utilisateur'
;
comment on column USER_TOKEN.TOKEN is 'Le jeton !'
;
comment on column USER_TOKEN.ACTION is 'Spécification de l''action précise autorisée, le cas échéant'
;
comment on column USER_TOKEN.ACTIONS_COUNT is 'Nombre d''utilisation du jeton'
;
comment on column USER_TOKEN.ACTIONS_MAX_COUNT is 'Nombre maximum d''utilisations du jeton autorisée (0 = pas de limite)'
;
comment on column USER_TOKEN.CREATED_ON is 'Date de création du jeton'
;
comment on column USER_TOKEN.EXPIRED_ON is 'Date d''expiration du jeton'
;
comment on column USER_TOKEN.LAST_USED_ON is 'Date de dernière utilisation du jeton'
;
comment on column USER_TOKEN.SENT_ON is 'Date d''envoi du jeton à l''utilisateur par mail'
;

create unique index USER_TOKEN_USER_ACTION_UN
    on USER_TOKEN (USER_ID, ACTION)
;

create sequence user_token_id_seq
;


--
-- Données : privilèges.
--

insert into CATEGORIE_PRIVILEGE(ID, CODE, LIBELLE, ORDRE)
values (CATEGORIE_PRIVILEGE_ID_SEQ.nextval, 'unicaen-auth-token', 'Jetons utilisateur', 10)
;

insert into PRIVILEGE(ID, CATEGORIE_ID, CODE, LIBELLE, ORDRE)
with d(ordre, code, lib) as (
    select 10, 'lister', 'Lister les jetons utilisateur' from dual union
    select 20, 'consulter', 'Consulter un jeton utilisateur' from dual union
    select 30, 'creer', 'Créer un jeton utilisateur' from dual union
    select 40, 'modifier', 'Modifier un jeton utilisateur' from dual union
    select 50, 'prolonger', 'Prolonger un jeton utilisateur' from dual union
    select 60, 'supprimer', 'Supprimer un jeton utilisateur' from dual union
    select 80, 'envoyer', 'Envoyer un jeton utilisateur par mail' from dual
)
select privilege_id_seq.nextval, cp.id, d.code, d.lib, d.ordre
from d
         join CATEGORIE_PRIVILEGE cp on cp.CODE = 'unicaen-auth-token'
;

insert into ROLE_PRIVILEGE (ROLE_ID, PRIVILEGE_ID)
with d(code_cat, code_priv) as (
    select 'unicaen-auth-token', 'lister' from dual union
    select 'unicaen-auth-token', 'consulter' from dual union
    select 'unicaen-auth-token', 'creer' from dual union
    select 'unicaen-auth-token', 'modifier' from dual union
    select 'unicaen-auth-token', 'prolonger' from dual union
    select 'unicaen-auth-token', 'supprimer' from dual union
    select 'unicaen-auth-token', 'envoyer' from dual
)
select r.id, p.id
from d
         join CATEGORIE_PRIVILEGE cp on cp.CODE = d.code_cat
         join PRIVILEGE p on cp.ID = p.CATEGORIE_ID and p.CODE = d.code_priv
         join role r on r.CODE = 'ADMIN_TECH'
;
