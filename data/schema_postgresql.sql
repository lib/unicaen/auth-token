--
-- Table, etc.
--

create table user_token
(
    id bigserial not null
        constraint user_token_pk
            primary key,
    user_id bigint not null,
    token varchar(256) not null,
    action varchar(256),
    actions_count smallint default 0 not null,
    actions_max_count smallint default 0 not null,
    created_on date default current_timestamp not null,
    expired_on date not null,
    last_used_on date,
    sent_on date
);
comment on table user_token is 'Jetons d''authentification utilisateur'
;
comment on column user_token.user_id is 'Identifiant unique de l''utilisateur'
;
comment on column user_token.token is 'Le jeton !'
;
comment on column user_token.action is 'Spécification de l''action précise autorisée, le cas échéant'
;
comment on column user_token.actions_count is 'Nombre d''utilisation du jeton'
;
comment on column user_token.actions_max_count is 'Nombre maximum d''utilisations du jeton autorisée (0 = pas de limite)'
;
comment on column user_token.created_on is 'Date de création du jeton'
;
comment on column user_token.expired_on is 'Date d''expiration du jeton'
;
comment on column user_token.last_used_on is 'Date de dernière utilisation du jeton'
;
comment on column user_token.sent_on is 'Date d''envoi du jeton à l''utilisateur par mail'
;

create unique index user_token_user_action_un
    on user_token (user_id, action)
;

create sequence user_token_id_seq
;


--
-- Données : privilèges.
--

insert into categorie_privilege(id, code, libelle, ordre)
values (categorie_privilege_id_seq.nextval, 'unicaen-auth-token', 'Jetons utilisateur', 10)
;

insert into privilege(id, categorie_id, code, libelle, ordre)
with d(ordre, code, lib) as (
    select 10, 'lister', 'Lister les jetons utilisateur' union
    select 20, 'consulter', 'Consulter un jeton utilisateur' union
    select 30, 'creer', 'Créer un jeton utilisateur' union
    select 40, 'modifier', 'Modifier un jeton utilisateur' union
    select 50, 'prolonger', 'Prolonger un jeton utilisateur' union
    select 60, 'supprimer', 'Supprimer un jeton utilisateur' union
    select 80, 'envoyer', 'Envoyer un jeton utilisateur par mail'
)
select privilege_id_seq.nextval, cp.id, d.code, d.lib, d.ordre
from d
         join categorie_privilege cp on cp.code = 'unicaen-auth-token'
;

insert into role_privilege (role_id, privilege_id)
with d(code_cat, code_priv) as (
    select 'unicaen-auth-token', 'lister' union
    select 'unicaen-auth-token', 'consulter' union
    select 'unicaen-auth-token', 'creer' union
    select 'unicaen-auth-token', 'modifier' union
    select 'unicaen-auth-token', 'prolonger' union
    select 'unicaen-auth-token', 'supprimer' union
    select 'unicaen-auth-token', 'envoyer'
)
select r.id, p.id
from d
         join categorie_privilege cp on cp.code = d.code_cat
         join privilege p on cp.id = p.categorie_id and p.code = d.code_priv
         join role r on r.code = 'ADMIN_TECH'
;
