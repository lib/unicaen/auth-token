<?php

namespace UnicaenAuthToken\Authentication\Adapter;

use RuntimeException;
use UnicaenAuthentification\Authentication\Adapter\AbstractDb;
use UnicaenAuthToken\Service\TokenServiceAwareTrait;
use UnicaenAuthToken\Service\TokenServiceException;
use Laminas\Authentication\Result as AuthenticationResult;
use ZfcUser\Entity\UserInterface;

/**
 * Adpater d'authentification à partir d'un token.
 *
 * @author Unicaen
 *
 * @property \UnicaenAuthToken\Options\ModuleOptions $moduleOptions
 */
class TokenAdapter extends AbstractDb
{
    use TokenServiceAwareTrait;

    const TYPE = 'token';
    protected $type = self::TYPE;

    /**
     * @inheritDoc
     */
    protected function fetchUserObject(): ?UserInterface
    {
        /** @var \Laminas\Http\Request $request */
        $request = $this->event->getRequest();
        $token = $request->getPost()->get('identity');

        $userToken= $this->tokenService->findUserTokenByToken($token);
        if ($userToken === null) {
            $this->event
                ->setCode(AuthenticationResult::FAILURE_IDENTITY_NOT_FOUND)
                ->setMessages([]);
            $this->setSatisfied(false);
            return null;
        }

        if ($userToken->isExpired()) {
            $this->event
                ->setCode(AuthenticationResult::FAILURE_UNCATEGORIZED)
                ->setMessages(["Ce jeton n'est plus utilisable car il a expiré."]);
            $this->setSatisfied(false);
            return null;
        }
        if ($userToken->isActionsMaxCountReached()) {
            $this->event
                ->setCode(AuthenticationResult::FAILURE_UNCATEGORIZED)
                ->setMessages(["Ce jeton n'est plus utilisable car il a atteint son nombre maximum d'utilisations."]);
            $this->setSatisfied(false);
            return null;
        }

        $userToken->incrementActionsCount();
        $userToken->setLastUsedOn();
        try {
            $this->tokenService->saveUserToken($userToken);
        } catch (TokenServiceException $e) {
            throw new RuntimeException("Impossible d'incrémenter le jeton.", null, $e);
        }

        $userObject = $this->mapper->findById($userToken->getUserId());
        if ($userObject === null) {
            throw new RuntimeException("Le jeton fait référence à un utilisateur introuvable.");
        }

        return $userObject;
    }

    /**
     * @inheritDoc
     */
    protected function authenticateUserObject(UserInterface $userObject): bool
    {
        return true;
    }
}