<?php

namespace UnicaenAuthToken\Authentication\Adapter;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Authentication\Adapter\Db as DbAdapter;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthToken\Service\TokenService;
use Laminas\Authentication\Storage\Session;
use ZfcUser\Mapper\UserInterface as UserMapperInterface;

class TokenAdapterFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return TokenAdapter
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null): TokenAdapter
    {
        /** @var UserMapperInterface $userMapper */
        $userMapper = $container->get('zfcuser_user_mapper');

        $adapter = new TokenAdapter();
        $adapter->setStorage(new Session(DbAdapter::class)); // NB: DbAdapter
        $adapter->setMapper($userMapper);

        $unicaenAuthModuleOptions = $container->get(ModuleOptions::class);
        $adapter->setModuleOptions($unicaenAuthModuleOptions);

        $tokenService = $container->get(TokenService::class);
        $adapter->setTokenService($tokenService);

        return $adapter;
    }
}