<?php

namespace UnicaenAuthToken\Controller;

use InvalidArgumentException;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\ViewModel;
use RuntimeException;
use UnexpectedValueException;
use UnicaenAuthToken\Mvc\RedirectResponse;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenAuthentification\Service\Traits\UserServiceAwareTrait;
use UnicaenAuthToken\Entity\Db\AbstractUserToken;
use UnicaenAuthToken\Form\UserTokenForm;
use UnicaenAuthToken\Service\TokenServiceAwareTrait;
use UnicaenAuthToken\Service\TokenServiceException;

/**
 * Class TokenController
 *
 * @method FlashMessenger flashMessenger()
 */
class TokenController extends AbstractActionController
{
    use TokenServiceAwareTrait;
    use UserServiceAwareTrait;

    const PROLONG_SPEC = '+1 week';
    const PROLONG_SPEC_TOSTRING = '1 semaine';

    const EVENT_TOKEN_CREATE_BEFORE_SAVE = 'EVENT_TOKEN_CREATE_BEFORE_SAVE';
    const EVENT_TOKEN_CREATE_AFTER_SAVE = 'EVENT_TOKEN_CREATE_AFTER_SAVE';
    const EVENT_TOKEN_MODIFY_BEFORE_SAVE = 'EVENT_TOKEN_MODIFY_BEFORE_SAVE';
    const EVENT_TOKEN_MODIFY_AFTER_SAVE = 'EVENT_TOKEN_MODIFY_AFTER_SAVE';
    const EVENT_TOKEN_DELETE_BEFORE = 'EVENT_TOKEN_DELETE_BEFORE';
    const EVENT_TOKEN_DELETE_AFTER = 'EVENT_TOKEN_DELETE_AFTER';
    const EVENT_TOKEN_PROLONGATE_BEFORE_SAVE = 'EVENT_TOKEN_PROLONGATE_BEFORE_SAVE';
    const EVENT_TOKEN_PROLONGATE_AFTER_SAVE = 'EVENT_TOKEN_PROLONGATE_AFTER_SAVE';

    /**
     * @var UserTokenForm
     */
    protected $form;

    /**
     * @var RedirectResponse
     */
    private $redirectResponse;

    /**
     * @var \Laminas\View\Renderer\PhpRenderer
     */
    private $renderer;

    /**
     * @param RedirectResponse $redirectResponse
     */
    public function setRedirectResponse(RedirectResponse $redirectResponse)
    {
        $this->redirectResponse = $redirectResponse;
    }

    /**
     * @param \UnicaenAuthToken\Form\UserTokenForm $form
     */
    public function setForm(UserTokenForm $form): void
    {
        $this->form = $form;
    }

    public function indexAction(): array
    {
        $userTokens = $this->tokenService->findAllUserToken();
        $this->tokenService->injectLogInUriInUserTokens($userTokens);

        return [
            'paginator' => $userTokens,
        ];
    }

    public function creerAction(): Response|ViewModel
    {
        $userId = $this->params('user');

        try {
            $userToken = $this->tokenService->createUserToken();
        } catch (TokenServiceException $e) {
            throw new RuntimeException("Erreur rencontrée lors de la création du jeton", null, $e);
        }

        if ($userId !== null) {
            if ($existingUserToken = $this->tokenService->findUserTokenByUserId($userId)) {
                throw new InvalidArgumentException(sprintf(
                    "Il existe déjà un jeton pour l'utilisateur %d (date d'expiration : %s).",
                    $existingUserToken->getUserId(),
                    $existingUserToken->getExpiredOnToString()
                ));
            }
            $user = $this->fetchUserById($userId);
            $userToken->setUser($user);
        }

        $this->form->bind($userToken);

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $this->form->setData($data);
            if ($this->form->isValid()) {
                $userToken = $this->form->getObject();
                try {
                    $this->triggerEvent(static::EVENT_TOKEN_CREATE_BEFORE_SAVE, ['userToken' => $userToken, 'data' => $data]);

                    $this->tokenService->saveUserToken($userToken);

                    $this->flashMessenger()->addSuccessMessage(sprintf(
                        "Un jeton a été créé avec succès pour l'utilisateur %d (date d'expiration : %s).",
                        $userToken->getUserId(),
                        $userToken->getExpiredOnToString()
                    ));

                    // un message mail par défaut est fourni dans l'événement, son envoi éventuel restant à la charge de l'appli.
                    try {
                        $mailMessage = $this->tokenService->createUserTokenMail($userToken);
                    } catch (\InvalidArgumentException $e) {
                        $mailMessage = null;
                    }

                    $response = $this->triggerEvent(static::EVENT_TOKEN_CREATE_AFTER_SAVE, ['userToken' => $userToken, 'data' => $data, 'mailMessage' => $mailMessage]);
                    if ($response instanceof Response) {
                        return $response;
                    }

                    $redirectCallback = $this->redirectResponse;

                    return $redirectCallback('unicaen-auth-token/token');

                } catch (TokenServiceException $e) {
                    $message = "La création du token a échoué. ";
                    error_log($message);
                    error_log($e->getTraceAsString());
                    $this->flashMessenger()->addErrorMessage($message);
                    $this->flashMessenger()->addErrorMessage($e->getMessage());
                }
            } else {
                $this->flashMessenger()->addErrorMessage("Les informations fournies ne sont pas valides.");
            }
        }

        $vm = new ViewModel([
            'userToken' => $userToken,
            'form' => $this->form,
        ]);
        $vm->setTemplate('unicaen-auth-token/token/edit');

        return $vm;
    }

    public function modifierAction()
    {
        $userToken = $this->getRequestedUserToken();

        $this->form->bind($userToken);

        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $this->form->setData($data);
            if ($this->form->isValid()) {
                $userToken = $this->form->getObject();
                try {
                    $this->triggerEvent(static::EVENT_TOKEN_MODIFY_BEFORE_SAVE, ['userToken' => $userToken, 'data' => $data]);

                    $this->tokenService->saveUserToken($userToken);

                    $this->flashMessenger()->addSuccessMessage(sprintf(
                        "Le jeton de l'utilisateur %d a été modifié avec succès.",
                        $userToken->getUserId()
                    ));

                    $response = $this->triggerEvent(static::EVENT_TOKEN_MODIFY_AFTER_SAVE, ['userToken' => $userToken, 'data' => $data]);
                    if ($response instanceof Response) {
                        return $response;
                    }

                    $redirectCallback = $this->redirectResponse;

                    return $redirectCallback('unicaen-auth-token/token');

                } catch (TokenServiceException $e) {
                    $message = "La modification du token a échoué. ";
                    error_log($message);
                    error_log($e->getTraceAsString());
                    $this->flashMessenger()->addErrorMessage($message);
                    $this->flashMessenger()->addErrorMessage($e->getMessage());
                }
            } else {
                $this->flashMessenger()->addErrorMessage("Les informations fournies ne sont pas valides.");
            }
        }

        $vm = new ViewModel([
            'userToken' => $userToken,
            'form' => $this->form,
        ]);
        $vm->setTemplate('unicaen-auth-token/token/edit');

        return $vm;
    }

    public function supprimerAction(): Response
    {
        $userToken = $this->getRequestedUserToken();
        try {
            $this->triggerEvent(static::EVENT_TOKEN_DELETE_BEFORE, ['userToken' => $userToken]);

            $this->tokenService->deleteUserToken($userToken);

            $this->flashMessenger()->addSuccessMessage(
                "Le jeton de l'utilisateur {$userToken->getUserId()} a été supprimé avec succès."
            );

            $response = $this->triggerEvent(static::EVENT_TOKEN_DELETE_AFTER, ['userToken' => $userToken]);
            if ($response instanceof Response) {
                return $response;
            }
        } catch (TokenServiceException $e) {
            throw new RuntimeException("Impossible de prolonger le jeton utilisateur spécifié.");
        }

        $redirectCallback = $this->redirectResponse;

        return $redirectCallback('unicaen-auth-token/token');
    }

    public function prolongerAction(): Response
    {
        $userToken = $this->getRequestedUserToken();
        $userToken->prolongate();
        try {
            $this->triggerEvent(static::EVENT_TOKEN_PROLONGATE_BEFORE_SAVE, ['userToken' => $userToken]);

            $this->tokenService->saveUserToken($userToken);

            $this->flashMessenger()->addSuccessMessage(sprintf(
                "La date d'expiration du jeton de l'utilisateur %d a été portée avec succès au %s.",
                $userToken->getUserId(),
                $userToken->getExpiredOnToString()
            ));

            $response = $this->triggerEvent(static::EVENT_TOKEN_PROLONGATE_AFTER_SAVE, ['userToken' => $userToken]);
            if ($response instanceof Response) {
                return $response;
            }
        } catch (TokenServiceException $e) {
            throw new RuntimeException("Impossible de prolonger le jeton utilisateur spécifié.");
        }

        $redirectCallback = $this->redirectResponse;

        return $redirectCallback('unicaen-auth-token/token');
    }

    /**
     * @deprecated
     */
    public function envoyerAction(): Response
    {
        $userToken = $this->getRequestedUserToken();
        $message = $this->tokenService->createUserTokenMail($userToken);

        $this->flashMessenger()->addErrorMessage(sprintf(
            "Le jeton utilisateur n'a pas été envoyé à %s, la bibliothèque n'envoie plus de mail !",
            $message->getTo()->rewind()->getEmail()
        ));

        $redirectCallback = $this->redirectResponse;

        return $redirectCallback('unicaen-auth-token/token');
    }

    protected function triggerEvent(string $eventName, array $params): ?Response
    {
        $eventManager = $this->events;
        $h = spl_object_hash($eventManager);
        $responseCollection = $eventManager->triggerUntil(function ($result) {
            return $result instanceof Response;
        }, $eventName, $this, $params);

        if ($responseCollection->stopped()) {
            if (($last = $responseCollection->last()) instanceof Response) {
                return $last;
            }
            throw new InvalidArgumentException(sprintf(
                "Événement '%s' stoppé sans résultat de type '%s' ('%s' reçu à la place).",
                $eventName,
                Response::class,
                is_object($last) ? get_class($last) : gettype($last)
            ));
        }

        return null;
    }

    /**
     * @return AbstractUserToken
     */
    private function getRequestedUserToken(): AbstractUserToken
    {
        $id = $this->params('userToken');
        if ($id === null) {
            throw new UnexpectedValueException("Un id de jeton utilisateur doit être spécifié dans la requête.");
        }

        $userToken = $this->tokenService->findUserTokenById($id);
        if ($userToken === null) {
            throw new UnexpectedValueException("Le jeton utilisateur spécifié dans la requête n'existe pas.");
        }

        return $userToken;
    }

    /**
     * @param int $id
     * @return \UnicaenUtilisateur\Entity\Db\UserInterface
     */
    private function fetchUserById(int $id): UserInterface
    {
        $user = $this->userService->getUserMapper()->findById($id);
        if ($user === null) {
            throw new UnexpectedValueException(sprintf("Aucun utilisateur trouvé avec l'id %d.", $id));
        }
        if (! $user instanceof UserInterface) {
            throw new UnexpectedValueException(sprintf(
                "Le service de recherche d'utilisateur doit retourner des instances de type %s.",
                UserInterface::class
            ));
        }

        return $user;
    }
}