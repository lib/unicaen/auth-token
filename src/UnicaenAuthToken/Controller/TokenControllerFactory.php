<?php

namespace UnicaenAuthToken\Controller;

use Interop\Container\ContainerInterface;
use UnicaenAuthToken\Mvc\RedirectResponse;
use UnicaenAuthToken\Form\UserTokenForm;
use UnicaenAuthToken\Service\TokenService;
use UnicaenAuthentification\Service\User as UserService;

class TokenControllerFactory
{
    /**
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TokenController
    {
        $controller = new TokenController();

        $tokenService = $container->get(TokenService::class);
        $controller->setTokenService($tokenService);

        /** @var UserService $userService */
        $userService = $container->get(UserService::class);
        $controller->setUserService($userService);

        $form = $container->get('FormElementManager')->get(UserTokenForm::class);
        $controller->setForm($form);

        /* @var RedirectResponse $redirectResponse */
        $redirectResponse = $container->get(RedirectResponse::class);
        $controller->setRedirectResponse($redirectResponse);

        return $controller;
    }
}