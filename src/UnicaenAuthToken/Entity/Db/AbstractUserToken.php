<?php

namespace UnicaenAuthToken\Entity\Db;

use DateTime;
use InvalidArgumentException;
use LogicException;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenAuthToken\Service\TokenService;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use Laminas\Uri\Uri;

abstract class AbstractUserToken implements ResourceInterface
{
    const RESOURCE_ID = 'UserToken';

    const DATETIME_FORMAT = 'd/m/Y à H:i';

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var string|null
     */
    protected $action;

    /**
     * @var int
     */
    protected $actionsCount;

    /**
     * @var int
     */
    protected $actionsMaxCount;

    /**
     * @var \DateTime
     */
    protected $createdOn;

    /**
     * @var \DateTime
     */
    protected $expiredOn;

    /**
     * @var \DateTime
     */
    protected $lastUsedOn;

    /**
     * @var \DateTime
     */
    protected $sentOn;

    /**
     * @var int
     */
    protected $userId;

    /**
     * @var \UnicaenUtilisateur\Entity\Db\UserInterface
     */
    protected $user;

    /**
     * @var \Laminas\Uri\Uri|null
     */
    protected $logInUri;

    /**
     * AbstractUserToken constructor.
     */
    public function __construct()
    {
        $this->actionsCount = 0;
        $this->createdOn = new DateTime('now');
        $this->actionsMaxCount = 0; // pas de limite
        $this->expiredOn = new DateTime('+1 week');
    }

    /**
     * @inheritDoc
     */
    public function getResourceId(): string
    {
        return self::RESOURCE_ID;
    }

    /**
     * @return \Laminas\Uri\Uri
     */
    public function getLogInUri(): Uri
    {
        if ($this->logInUri === null) {
            throw new LogicException(sprintf(
                "Impossible d'appeler la méthode %s si l'URI d'authentification n'est pas au préalable injectée dans l'entité (cf. %s).",
                __METHOD__,
                TokenService::class
            ));
        }

        return $this->logInUri;
    }

    /**
     * @param string $redirect
     * @return \Laminas\Uri\Uri|null
     */
    public function getLogInUriWithRedirect(string $redirect): ?Uri
    {
        $uri = $this->getLogInUri();
        $uriQuery = $uri->getQueryAsArray();
        $uriQuery['redirect'] = $redirect;
        $uri->setQuery($uriQuery);

        return $uri;
    }

    /**
     * @param Uri $logInUri
     * @return self
     */
    public function setLogInUri(Uri $logInUri): self
    {
        $this->logInUri = $logInUri;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return self
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param \UnicaenUtilisateur\Entity\Db\UserInterface $user
     * @return self
     */
    public function setUser(UserInterface $user): self
    {
        $this->user = $user;
        $this->userId = $user->getId();

        return $this;
    }

    /**
     * @return \UnicaenUtilisateur\Entity\Db\UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getUserToString(): ?string
    {
        return (string) $this->user;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return self
     */
    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string|null $action
     * @return self
     */
    public function setAction(string $action = null): self
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return int
     */
    public function getActionsCount(): int
    {
        return $this->actionsCount;
    }

    /**
     * @return string
     */
    public function getActionsCountToString(): string
    {
        return $this->actionsCount . ' fois';
    }

    /**
     * @param int $actionsCount
     * @return self
     */
    public function setActionsCount(int $actionsCount): self
    {
        $this->actionsCount = $actionsCount;
        return $this;
    }

    /**
     * @return self
     */
    public function incrementActionsCount(): self
    {
        $this->actionsCount++;
        return $this;
    }

    /**
     * @return int
     */
    public function getActionsMaxCount(): int
    {
        return $this->actionsMaxCount;
    }

    /**
     * @return string
     */
    public function getActionsMaxCountToString(): string
    {
        return $this->actionsMaxCount > 0 ? ($this->actionsMaxCount . ' fois') : "Illimité";
    }

    /**
     * @param int $actionsMaxCount
     * @return self
     */
    public function setActionsMaxCount(int $actionsMaxCount): self
    {
        $this->actionsMaxCount = $actionsMaxCount;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActionsMaxCountReached(): bool
    {
        if ($this->actionsMaxCount <= 0) {
            return false;
        }

        return $this->actionsCount >= $this->actionsMaxCount;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedOn(): DateTime
    {
        return $this->createdOn;
    }

    /**
     * @return string
     */
    public function getCreatedOnToString(): string
    {
        return $this->createdOn ? $this->createdOn->format(static::DATETIME_FORMAT) : '';
    }

    /**
     * @param \DateTime|null $createdOn null = now
     * @return self
     */
    public function setCreatedOn(DateTime $createdOn = null): self
    {
        $this->createdOn = $createdOn ?: new DateTime('now');
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpiredOn(): DateTime
    {
        return $this->expiredOn;
    }

    /**
     * @return string
     */
    public function getExpiredOnToString(): string
    {
        return $this->expiredOn ? $this->expiredOn->format(static::DATETIME_FORMAT) : '';
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        if ($this->expiredOn === null) {
            return false;
        }

        return new DateTime('now') > $this->expiredOn;
    }

    /**
     * @param \DateTime $expiredOn
     * @return self
     */
    public function setExpiredOn(DateTime $expiredOn): self
    {
        $this->expiredOn = $expiredOn;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultProlongationSpec(): string
    {
        return '+1 week';
    }

    /**
     * @return string
     */
    public function getDefaultProlongationSpecToString(): string
    {
        return '1 semaine';
    }

    /**
     * @return self
     */
    public function prolongate(): self
    {
        if ($this->isExpired()) {
            $this->setExpiredOn((new DateTime())->setTime(23, 59, 59));
        }

        return $this->modifyExpiredOn($this->getDefaultProlongationSpec());
    }

    /**
     * @param string $spec Ex: '+1 day', '+1 hour'
     * @return self
     */
    public function modifyExpiredOn(string $spec): self
    {
        $result = $this->expiredOn->modify($spec);
        if ($result === false) {
            throw new InvalidArgumentException("Impossible de modifier la date avec ça : " . $spec);
        }
        $this->setExpiredOn(clone $result); // nouvelle référence obligatoire
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastUsedOn(): ?DateTime
    {
        return $this->lastUsedOn;
    }

    /**
     * @return string
     */
    public function getLastUsedOnToString(): string
    {
        return $this->lastUsedOn ? $this->lastUsedOn->format(static::DATETIME_FORMAT) : '';
    }

    /**
     * @param \DateTime|null $lastUsedOn null = now
     * @return self
     */
    public function setLastUsedOn(DateTime $lastUsedOn = null): self
    {
        $this->lastUsedOn = $lastUsedOn ?: new DateTime('now');
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getSentOn(): ?DateTime
    {
        return $this->sentOn;
    }

    /**
     * @return string
     */
    public function getSentOnToString(): string
    {
        return $this->sentOn ? $this->sentOn->format(static::DATETIME_FORMAT) : '';
    }

    /**
     * @param \DateTime $sentOn
     * @return self
     */
    public function setSentOn(DateTime $sentOn): self
    {
        $this->sentOn = $sentOn;
        return $this;
    }
}