<?php

namespace UnicaenAuthToken\Form;

use UnicaenAuthentification\Form\LoginForm;
use ZfcUser\Options\AuthenticationOptionsInterface;

class TokenLoginForm extends LoginForm
{
    protected $types = ['token'];
    protected $hidden = true;

    /**
     * @param $name
     * @param AuthenticationOptionsInterface $options
     */
    public function __construct($name, AuthenticationOptionsInterface $options)
    {
        parent::__construct($name, $options);

        $this->remove('credential');

        $this->get('identity')->setLabel("Token d'authentification");
        $this->get('submit')->setLabel("S'authentifier avec ce jeton");
    }

    /**
     * @inheritDoc
     */
    public function initFromRequest(\Laminas\Http\Request $request)
    {
        parent::initFromRequest($request);

        $this->get('identity')->setValue($request->getQuery('token'));
    }
}
