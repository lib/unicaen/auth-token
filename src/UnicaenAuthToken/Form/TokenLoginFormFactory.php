<?php

namespace UnicaenAuthToken\Form;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class TokenLoginFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): TokenLoginForm
    {
        $options = $container->get('zfcuser_module_options');

        return new TokenLoginForm(null, $options);
    }
}
