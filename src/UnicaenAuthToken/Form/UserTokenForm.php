<?php

namespace UnicaenAuthToken\Form;

use UnicaenAuthToken\Entity\Db\AbstractUserToken;
use Laminas\Form\Element\Csrf;
use Laminas\Form\Element\Date;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\Form\FormInterface;
use Laminas\InputFilter\InputFilterProviderInterface;

/**
 * Class UserTokenForm
 *
 * @method AbstractUserToken getObject()
 */
class UserTokenForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var string[]
     */
    protected $durations = [
        '+1 day' => '1 jour',
        '+2 days' => '2 jours',
        '+1 week' => '1 semaine',
        '+2 weeks' => '2 semaines',
        '+1 month' => '1 mois',
        '+2 months' => '2 mois',
    ];

    /**
     * @var bool
     */
    protected $userIdEditable = true;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->setAttribute('method', 'post');

        $factory = $this->getFormFactory();

        $this->add($factory->createElement([
            'type' => Text::class,
            'name' => 'userId',
            'options' => [
                'label' => "Id de l'utilisateur :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'userId',
                'class' => 'form-control',
            ],
        ]));

        $this->add($factory->createElement([
            'type' => Text::class,
            'name' => 'action',
            'options' => [
                'label' => 'Action concernée (NON IMPLÉMENTÉE) :',
                'label_attributes' => [
                ],
            ],
            'attributes' => [
                'id' => 'action',
                'class' => 'form-control',
            ],
        ]));

        $this->add($factory->createElement([
            'type' => Select::class,
            'name' => 'duration',
            'options' => [
                'label' => 'Durée de validité :',
                'label_attributes' => [
                    'class' => 'required',
                ],
                'value_options' => $this->durations,
                'empty_option' => "Sélectionner...",
                'disable_inarray_validator' => true,
            ],
            'attributes' => [
                'id' => 'duration',
                'class' => 'form-control',
            ],
        ]));

        $this->add($factory->createElement([
            'type' => Date::class,
            'name' => 'expiredOn',
            'options' => [
                'label' => "Date d'expiration (à 23:59:59) :",
                'label_attributes' => [
                    'class' => 'required',
                ],
                //'format' => 'd/m/Y', // illusoire semble-t-il
            ],
            'attributes' => [
                'id' => 'expiredOn',
                'class' => 'form-control',
            ],
        ]));

        $this->add(new Csrf('security'));

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'value' => 'Enregistrer',
            ],
        ]);
    }

    /**
     * @param AbstractUserToken $object
     * @param int $flags
     * @return self
     */
    public function bind($object, $flags = FormInterface::VALUES_NORMALIZED): self
    {
        $this->userIdEditable = $object->getUser() === null;

        return parent::bind($object, $flags);
    }

    /**
     * @inheritDoc
     */
    public function prepare()
    {
        if (! $this->userIdEditable) {
            $this->get('userId')->setValue($this->getObject()->getUser()->getId());
        }

        return parent::prepare();
    }

    /**
     * @return bool
     */
    public function isUserIdEditable(): bool
    {
        return $this->userIdEditable;
    }

    /**
     * @return bool
     */
    protected function isCreation(): bool
    {
        return $this->getObject()->getId() === null;
    }

    /**
     * @inheritDoc
     */
    public function getInputFilterSpecification(): array
    {
        return [
            'userId' => [
                'required' => true,
            ],
            'action' => [
                'required' => false,
            ],
            'duration' => [
                'required' => $this->isCreation(),
            ],
            'expiredOn' => [
                'required' => ! $this->isCreation(),
            ],
        ];
    }
}