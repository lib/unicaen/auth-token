<?php

namespace UnicaenAuthToken\Form;

use Interop\Container\ContainerInterface;
use UnicaenAuthToken\Hydrator\UserTokenHydrator;

class UserTokenFormFactory
{
    public function __invoke(ContainerInterface $container): UserTokenForm
    {
        $hydrator = $container->get('HydratorManager')->get(UserTokenHydrator::class);

        $form = new UserTokenForm();
        $form->setHydrator($hydrator);

        return $form;
    }
}
