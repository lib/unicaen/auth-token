<?php

namespace UnicaenAuthToken\Hydrator;

use DateTime;
use Doctrine\Laminas\Hydrator\DoctrineObject;
use InvalidArgumentException;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenAuthentification\Service\Traits\UserServiceAwareTrait;
use UnicaenAuthToken\Entity\Db\AbstractUserToken;

class UserTokenHydrator extends DoctrineObject
{
    use UserServiceAwareTrait;

    /**
     * @var string
     */
    private $dateFormat = 'Y-m-d';

    /**
     * @param string $dateFormat
     * @return self
     */
    public function setDateFormat(string $dateFormat): self
    {
        $this->dateFormat = $dateFormat;
        return $this;
    }

    /**
     * @param array $data
     * @param AbstractUserToken $object
     * @return AbstractUserToken
     * @throws \Exception
     */
    public function hydrate(array $data, $object): AbstractUserToken
    {
        if (isset($data['duration'])) {
            $data['expiredOn'] = new DateTime($data['duration']);
        } else {
            $data['expiredOn'] = DateTime::createFromFormat($this->dateFormat, $data['expiredOn']);
        }
        $data['expiredOn']->setTime(23, 59, 59);

        /** @var AbstractUserToken $object */
        $object = parent::hydrate($data, $object);
        $object->setExpiredOn($data['expiredOn']);
        $object->setUser($this->fetchUserById($data['userId']));

        return $object;
    }

    /**
     * @param int $id
     * @return \UnicaenUtilisateur\Entity\Db\UserInterface
     */
    private function fetchUserById(int $id): UserInterface
    {
        $user = $this->userService->getUserMapper()->findById($id);
        if ($user === null) {
            throw new InvalidArgumentException(sprintf("Aucun utilisateur trouvé avec l'id %d.", $id));
        }
        if (! $user instanceof UserInterface) {
            throw new InvalidArgumentException(sprintf(
                "Le service de recherche d'utilisateur doit retourner des instances de type %s.",
                UserInterface::class
            ));
        }

        return $user;
    }
}