<?php

namespace UnicaenAuthToken\Hydrator;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Service\User as UserService;

class UserTokenHydratorFactory
{
    public function __invoke(ContainerInterface $container): UserTokenHydrator
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');

        $hydrator = new UserTokenHydrator($em);

        /** @var UserService $userMapper */
        $userService = $container->get(UserService::class);
        $hydrator->setUserService($userService);

        return $hydrator;
    }
}