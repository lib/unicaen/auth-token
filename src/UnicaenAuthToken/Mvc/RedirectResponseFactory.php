<?php

namespace UnicaenAuthToken\Mvc;

use Psr\Container\ContainerInterface;
use Laminas\Mvc\Application;
use Laminas\Router\RouteInterface;

class RedirectResponseFactory
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RedirectResponse
    {
        /* @var RouteInterface $router */
        $router = $container->get('Router');

        /* @var Application $application */
        $application = $container->get('Application');

        return new RedirectResponse($application, $router);
    }
}
