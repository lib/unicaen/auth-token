<?php

namespace UnicaenAuthToken\Options;

use UnicaenAuthToken\Entity\Db\AbstractUserToken;

/**
 * Classe encapsulant les options de fonctionnement du module unicaen/auth-token.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class ModuleOptions
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var string
     */
    protected $userTokenEntityClass;

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param array $config
     * @return self
     */
    public function setConfig(array $config): self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserTokenEntityClass(): string
    {
        return $this->userTokenEntityClass;
    }

    /**
     * @param string $userTokenEntityClass
     * @return self
     */
    public function setUserTokenEntityClass(string $userTokenEntityClass): self
    {
        if (! is_subclass_of($userTokenEntityClass, AbstractUserToken::class)) {
            throw new \InvalidArgumentException("La classé d'entité doit hériter de " . AbstractUserToken::class);
        }

        $this->userTokenEntityClass = $userTokenEntityClass;
        return $this;
    }
}