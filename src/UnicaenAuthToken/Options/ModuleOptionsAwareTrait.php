<?php

namespace UnicaenAuthToken\Options;

trait ModuleOptionsAwareTrait
{
    /**
     * @var \UnicaenAuthToken\Options\ModuleOptions
     */
    protected $moduleOptions;

    /**
     * @param \UnicaenAuthToken\Options\ModuleOptions $moduleOptions
     */
    public function setModuleOptions(ModuleOptions $moduleOptions)
    {
        $this->moduleOptions = $moduleOptions;
    }
}