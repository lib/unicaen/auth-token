<?php

namespace UnicaenAuthToken\Options;

use Interop\Container\ContainerInterface;

/**
 * @author Unicaen
 */
class ModuleOptionsFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @return ModuleOptions
     */
    public function __invoke(ContainerInterface $container): ModuleOptions
    {
        $moduleOptions = new ModuleOptions();

        $unicaenAuthConfig = $container->get('unicaen-auth_module_options');
        $tokenConfig = $unicaenAuthConfig->__get('token');
        $moduleOptions->setConfig($tokenConfig);

        $config = $container->get('Config');
        $moduleConfig = $config['unicaen-auth-token'];
        $moduleOptions->setUserTokenEntityClass($moduleConfig['user_token_entity_class']);

        return $moduleOptions;
    }
}