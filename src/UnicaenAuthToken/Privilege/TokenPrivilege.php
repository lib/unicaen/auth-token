<?php

namespace UnicaenAuthToken\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class TokenPrivilege extends Privileges
{
    const LISTER = 'unicaen-auth-token-lister';
    const CONSULTER = 'unicaen-auth-token-consulter';
    const CREER = 'unicaen-auth-token-creer';
    const MODIFIER = 'unicaen-auth-token-modifier';
    const SUPPRIMER = 'unicaen-auth-token-supprimer';
    const PROLONGER = 'unicaen-auth-token-prolonger';
    const ENVOYER = 'unicaen-auth-token-envoyer';
}