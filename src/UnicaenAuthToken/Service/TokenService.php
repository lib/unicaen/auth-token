<?php

namespace UnicaenAuthToken\Service;

use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Exception;
use Laminas\Mail\Message;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mime\Mime;
use Laminas\Mime\Part;
use Laminas\Router\RouteInterface;
use Laminas\Uri\Uri;
use Laminas\View\Renderer\PhpRenderer;
use Ramsey\Uuid\Uuid;
use UnicaenAuthToken\Entity\Db\AbstractUserToken;
use UnicaenAuthToken\Options\ModuleOptionsAwareTrait;

class TokenService
{
    use ModuleOptionsAwareTrait;

    const PASSWORD_RESET_TOKEN_SEP = '-';
    const PASSWORD_RESET_TOKEN_DATE_FORMAT = 'YmdHis';

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Spécifie le gestionnaire d'entité.
     *
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @var \Laminas\Router\RouteInterface
     */
    protected $router;

    /**
     * @param \Laminas\Router\RouteInterface $router
     */
    public function setRouter(RouteInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @var \Laminas\View\Renderer\PhpRenderer
     */
    protected $phpRenderer;

    /**
     * @param \Laminas\View\Renderer\PhpRenderer $phpRenderer
     */
    public function setPhpRenderer(PhpRenderer $phpRenderer): void
    {
        $this->phpRenderer = $phpRenderer;
    }

    protected function getRepository(): EntityRepository
    {
        return $this->entityManager->getRepository($this->moduleOptions->getUserTokenEntityClass());
    }

    public function createUserToken(DateTime $expiredOn = null): AbstractUserToken
    {
        try {
            $uuid = $this->generateUuid();
        } catch (Exception $e) {
            throw TokenServiceException::generateUuidError($e);
        }

        $class = $this->moduleOptions->getUserTokenEntityClass();
        $userToken = new $class();
        $userToken->setToken($uuid);
        if ($expiredOn !== null) {
            $userToken->setExpiredOn($expiredOn);
        }

        return $userToken;
    }

    /**
     * @throws \UnicaenAuthToken\Service\TokenServiceException
     */
    public function saveUserToken(AbstractUserToken $userToken)
    {
        try {
            $this->entityManager->persist($userToken);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            throw TokenServiceException::saveUserTokenError($e);
        }
    }

    /**
     * @param int $id
     * @return AbstractUserToken
     */
    public function findUserTokenById(int $id): AbstractUserToken
    {
        /** @var AbstractUserToken $userToken */
        $userToken = $this->getRepository()->find($id);

        return $userToken;
    }

    /**
     * @param string $token
     * @return AbstractUserToken|null
     */
    public function findUserTokenByToken(string $token): ?AbstractUserToken
    {
        /** @var AbstractUserToken $userToken */
        $userToken = $this->getRepository()->findOneBy(['token' => $token]);

        return $userToken;
    }

    /**
     * @param int $userId
     * @return AbstractUserToken|null
     */
    public function findUserTokenByUserId(int $userId): ?AbstractUserToken
    {
        /** @var AbstractUserToken $userToken */
        $userToken = $this->getRepository()->findOneBy(['userId' => $userId], ['createdOn' => 'ASC']);

        return $userToken;
    }

    /**
     * @param int $userId
     * @return AbstractUserToken[]
     */
    public function findUserTokensByUserId(int $userId): array
    {
        $qb = $this->getRepository()->createQueryBuilder('token')
            ->andWhere('token.userId = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('token.expiredOn', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return AbstractUserToken[]
     */
    public function findAllUserToken(): array
    {
        $qb = $this->getRepository()->createQueryBuilder('ut')
            ->addOrderBy('ut.userId');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param \UnicaenAuthToken\Entity\Db\AbstractUserToken $userToken
     * @throws \UnicaenAuthToken\Service\TokenServiceException
     */
    public function deleteUserToken(AbstractUserToken $userToken)
    {
        try {
            $this->entityManager->remove($userToken);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            throw TokenServiceException::deleteUserTokenError($e);
        }
    }

    public function createUserTokenMail(AbstractUserToken $userToken, string $partial = 'unicaen-auth-token/token/partial/mail'): Message
    {
        if ($userToken->getUser()->getEmail() === null) {
            throw new \InvalidArgumentException("Le propriétaire du jeton n'a pas d'adresse mail");
        }

        $this->injectLogInUriInUserTokens([$userToken]);
        $html = $this->phpRenderer->render($partial, ['userToken' => $userToken]);

        $part = new Part($html);
        $part->type = Mime::TYPE_HTML;
        $part->charset = 'UTF-8';
        $body = new MimeMessage();
        $body->addPart($part);

        $mail = (new Message())
            ->setEncoding('UTF-8')
            ->setSubject("Un jeton d'authentification vous a été attribué")
            ->setBody($body);

        $mail->setTo($userToken->getUser()->getEmail());

        return $mail;
    }

    public function createNewMessage($htmlBody, $subject)
    {
        $html = $htmlBody;
        $part = new Part($html);
        $part->type = Mime::TYPE_HTML;
        $part->charset = 'UTF-8';
        $body = new MimeMessage();
        $body->addPart($part);

        return (new Message())
            ->setEncoding('UTF-8')
//            ->setFrom($from)
            ->setSubject($subject)
            ->setBody($body);
    }

    /**
     * Génération d'un token.
     *
     * @return string
     * @throws \Exception Erreur rencontrée lors de la génération du UUID
     */
    public function generateUuid(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * Calcule l'{@see Uri} de la page permettant de s'authentifier avec le jeton utilisateur spécifié.
     *
     * @param \UnicaenAuthToken\Entity\Db\AbstractUserToken $userToken
     * @param string|null $redirect
     * @return \Laminas\Uri\Uri
     */
    private function computeLogInUriForUserToken(AbstractUserToken $userToken, string $redirect = null): Uri
    {
        $query = array_filter([
            'token' => $userToken->getToken(),
            'redirect' => $redirect,
        ]);

        $redirectUrl = $this->router->assemble(['type' => 'token'], [
            'name' => 'zfcuser/login',
            'query' => $query,
            'force_canonical' => true,
        ]);

        return new Uri($redirectUrl);
    }

    /**
     * @param AbstractUserToken[] $userTokens
     * @param string|null $redirect URL ou route où rediriger après connexion
     */
    public function injectLogInUriInUserTokens(array $userTokens, string $redirect = null)
    {
        foreach ($userTokens as $userToken) {
            $uri = $this->computeLogInUriForUserToken($userToken, $redirect);
            $userToken->setLogInUri($uri);
        }
    }
}