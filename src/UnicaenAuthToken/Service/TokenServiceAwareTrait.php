<?php

namespace UnicaenAuthToken\Service;

trait TokenServiceAwareTrait
{
    /**
     * @var TokenService
     */
    protected $tokenService;

    /**
     * @param \UnicaenAuthToken\Service\TokenService $tokenService
     */
    public function setTokenService(TokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }
}