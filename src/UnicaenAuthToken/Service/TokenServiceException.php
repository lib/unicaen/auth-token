<?php

namespace UnicaenAuthToken\Service;

use Exception;
use Throwable;
use UnicaenUtilisateur\Entity\Db\AbstractUser;

class TokenServiceException extends Exception
{
    static public function generateUuidError(Throwable $previous): self
    {
        return new self("Erreur rencontrée lors de la génération du UUID.", null, $previous);
    }

    static public function deleteUserTokenError(Throwable $previous): self
    {
        return new self("Erreur rencontrée lors de la suppression du token utilisateur en bdd.", null, $previous);
    }

    static public function saveUserTokenError(Throwable $previous): self
    {
        return new self("Erreur rencontrée lors de l'enregistrement en bdd.", null, $previous);
    }
}