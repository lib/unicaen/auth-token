<?php

namespace UnicaenAuthToken\Service;

use Interop\Container\ContainerInterface;
use Laminas\Router\RouteInterface;
use UnicaenAuthToken\Options\ModuleOptions;

class TokenServiceFactory
{
    /**
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container): TokenService
    {
        $service = new TokenService();

        $moduleOptions = $container->get(ModuleOptions::class);
        $service->setModuleOptions($moduleOptions);

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $container->get('doctrine.entitymanager.orm_default');
        $service->setEntityManager($em);

        /** @var RouteInterface $router */
        $router = $container->get('router');
        $service->setRouter($router);

        /** @var \Laminas\View\Renderer\PhpRenderer $viewRenderer */
        $viewRenderer = $container->get('ViewRenderer');
        $service->setPhpRenderer($viewRenderer);

        return $service;
    }
}