<?php

namespace UnicaenAuthToken\View\Helper;

use UnicaenAuthentification\View\Helper\AbstractConnectViewHelper;
use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue dessinant le champ texte du token et le bouton de connexion.
 *
 * @method PhpRenderer getView()
 * @author Unicaen
 */
class TokenConnectViewHelper extends AbstractConnectViewHelper
{
    public function __construct()
    {
        $this->setType('token');
        $this->setTitle("Authentification par token");
    }
}