<?php

namespace UnicaenAuthToken\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuthToken\Options\ModuleOptions;

class TokenConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return TokenConnectViewHelper
     */
    public function __invoke(ContainerInterface $container): TokenConnectViewHelper
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get(ModuleOptions::class);
        $config = $moduleOptions->getConfig();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $description = $config['description'] ?? null;

        $helper = new TokenConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setDescription($description);

        return $helper;
    }
}